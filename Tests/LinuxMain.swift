import XCTest

import SequencesTests

var tests = [XCTestCaseEntry]()
tests += SequencesTests.__allTests()

XCTMain(tests)
